\documentclass[a4paper]{article}

%Miscalenious
\author{Léo Malli}
\title{Complex Analysis 2022\\ Recap of exercise}

%General packages
\usepackage[hidelinks]{hyperref}
\usepackage[margin=3cm, includefoot]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{bm}
\usepackage{tikz}
\usetikzlibrary{decorations.pathmorphing}

\usepackage{wrapfig}
%My Math shortcuts etc.
\input{\string~/.config/latex/functions.tex}
\newcommand{\conj}[1]{\bar{#1}}
\renewcommand{\Re}{\text{Re}}

%Set that the subsubsection are not numbered
\setcounter{secnumdepth}{2}


\begin{document}
%Title page and Table of contents
\begin{titlepage}
\maketitle
\thispagestyle{empty}
\end{titlepage}
\pagenumbering{roman}
\cleardoublepage
\pagenumbering{arabic}
\setcounter{page}{1}

%Beginning of the document
\paragraph{Preambule:}
I will use the space here to clarify some of the notations.\bigbreak

\begin{itemize}[label=--]
\item
    I use the convention where $\N$ contains all the naturals \emph{including 0}, i.e. $\N:=\{0,1,2,\dots\}$.
\end{itemize}

\section{Week 2}
\subsection{Basics on complex numbers}
In this section, except if stated otherwise, let $z:= x + iy$ be a complex number, i.e. $z\in\C$ and $x,y\in\R$.
\paragraph{Modulus:}
The \emph{modulus} $\bm{|z|}$ is defined as $\disp\sqrt{x^2 + y^2}$.
We have some basics properties, let $z,w\in\C$:
\begin{gather*}
    |z\cdot w| = |z|\cdot|w|\\
    z\cdot \conj{z} = |z|^2 \implies \frac{1}{z} = \frac{\conj{z}}{|z|^2}
\end{gather*}


\paragraph{Argument:}
The \emph{argument} $\bm{\arg(z)}$ is defined as the angle of the vector $(x\;y)^T\in\R^2$ with the positive real-axis.
The angle is to be taken in the trigonometric direction (counter-clockwise).
One usualy work with angles in $[0,2\pi)$ or in $[-\pi, \pi)$.
We have the following elementary properties, let $z,w\in\C$:
\begin{gather*}
    \arg(z\cdot w) = \arg(z) + \arg(w) \tag{$\ast$}\\
    z = |z|\big(\cos\arg(z) + i\sin\arg(z)\big)
\end{gather*}
$(\ast)$: Usually one work in the quotient group $\R/2\pi\Z$ so that addition of arguments and multiplication by a natural is well behaved.

\paragraph{Root of Unity (de Moivre number):}
A $n$-th root of the unity is any $z\in\C$ such that $z^n = 1$.

The set $\{ z\in\C \mid \exists n\in\N \text{ s.t. } z^n = 1\}$ endowed with the multiplication form an abelian group.

For some fixed $n\in\N$, the set $\{ z\in\C\mid z^n = 1 \}$ also endowed with the multiplication form a finite abelian group.
It is also cyclic of order $n$.

\paragraph{The complex exponential:}
Recal that if $\theta\in[0,2\pi)$, then $\disp e^{i\theta} = \cos\theta + i\sin\theta$.
It lets us write the $\sin$ and $\cos$ function in term of the exponential:
\begin{flalign*}
    && \cos\theta &= \frac{e^{i\theta} + e^{-i\theta}}{2} & \sin\theta &= \frac{e^{i\theta} - e^{-i\theta}}{2} &&
\end{flalign*}


\subsection{On Cauchy-Riemann}
Recall that in the theorem, the function $\fonc{\hat f}{U}{\C}$ must be differentiable in order to use the tools (e.g. the Cauchy-Riemann equations).
The differentiability is to be understood in the multivariate sense, otherwise we could construct easily some counter-examples to the Cauchy-Riemann thm/lemma.
For instance take:
\[
f(z) := \dfrac{z^5}{|z|^4}
\]
One can check easily that $f$ is continuous, the partial derivatives with respect to $x,y$ exist and that, at $z=0$, the Cauchy-Riemann equations are satisfied.
But this function is not holomorphic (on $\C$).


\subsubsection{Small explicit computation}
Let $\fonc{f}{\C}{\C}$ be the complex function defined by $f(z) := z^2$ (which is clearly holomorphic since it is simply a polynomial).
We want to compute explicitely $\hat f$ (i.e. the functions $u$ and $v$).
To this goal, rewrite $z=x + iy$ and compute:
\[
f(z) = z^2 = (x+iy)^2 = x^2 + (iy)^2 + i \cdot 2xy = x^2 - y^2 + i\cdot 2xy
\]
meaning we get $u(x, y) = x^2 - y^2$ and $v(x,y) = 2xy$.
We can then verify explicitely the Cauchy-Riemann equation:
\begin{gather*}
    \partial_x u = 2x = \partial_y v\\
    \partial_y u = -2y = -\partial_x v
\end{gather*}

\section{Week 3}
\subsection{Common errors}
\paragraph{Argument of $z$:} I should have been more explicit on how to compute it.
If $z:=x+iy$, the formula given in the lecture was $\arctan\left(y/x\right)$.
Note that the formula does not work in general!
Take for instance $z = -1$, the formula yields $\arg(z) = \arctan(0) = 0$.
You can directly see that it is not consitant with $|z|\cdot e^{i\arg(z)} = z$ as $-1\neq e^{i\cdot 0} = 1$.
Taking a look at $\arctan(x)$, you see that it is defined from $\R$ to $(-\pi/2, \pi/2)$, we are not taking into account the whole range of angles.

In general I strongly suggest that you sketch quickly where your complex number is on the plane to not make such silly mistake.
To get the exact argument, just add $\pi$ to the result whenever $\Re(z) < 0$, E.g. for $z = -1 - i$:
\[
    \arg(z) = \arctan\left(\frac{-1}{-1}\right) \bm{+ \pi} = \frac{\pi}{4} + \pi = \frac{5\pi}{4}
\]


\paragraph{Ordering the complex plane:}
The complex plane cannot be endowed with a meaningfull order relationship.
Meaning we \emph{cannot write such things as} $z < w$, or $z^2 > 0$, if $z, w\in\C$!!

Argument goes as follows, suppose $<$ is a total ordering of the complex numbers.
Either $0<i$, or $i < 0$.
If $0<i$, then $0\cdot i < i\cdot i \implies 0 < -1$.
Then $0 < (-1)^2 = 1$, which gives us:
\[
    0 < -1 \implies 1 = 0 + 1  < -1 + 1 = 0 \implies 1 < 0 < 1 \quad\text{\bfseries --- nonsensical}
\]
Same reasonning if we suppose $i < 0$.

\paragraph{Connectedness (in $\R^2$):}
Lots of question on how to prove or disprove connectedness in the series.
Since we will be working mostly with "easy" subset of $\R^2$, let us introduce the concept of path connectedness.


We say $A\subset \R^2$ is \emph{path connected} if $\forall x, y\in A$, there exists a "path" from $x$ to $y$.\bigskip


Without going to much into the definition of a path, if you find a continuous mapping $\fonc{\gamma}{[a,b]}{\R^2}$ with $\gamma(a) = x$, $\gamma(b) = y$ and $\gamma([a,b])\subset A$, it is sufficient.
Then if $A$ is path connected, then it is connected.
Note that the converse is not true --- see \emph{Long line, Topologist's sine curve}.

\paragraph{Notations:}
Usually we use $n$, or $k$ as natural or whole number implicitely.
When writing $\{1/n\mid n\geq 1\}$, we implicitely mean $\{1/n\mid n\geq 1, n\in\N\}$.
If we want real numbers, one usually use $x$ or $y$, similarly $z, w$ are usually used as complex numbers.
The meaning should be clear with the context, if not specified and/or unclear, just ask.


I've seen a lot of confusion with $(c, d)\neq (0,0)$ this is a inequality in $\R^2$.
It does not mean that $c\neq 0$, or that $d\neq 0$, e.g. $(c, d):=(1, 0)\neq (0, 0)$, but $d = 0$.


\subsection{A version of Morera's theorem}
This is sort of a converse of Cauchy's theorem.
The statement goes as follows:\bigskip


Let $A\subset\C$ be any open and path connected subset, $\fonc{f}{A}{\C}$ continuous such that for every closed curve $\gamma$ contained in $A$ you have
\[
    \int_\gamma f(z)\;dx = 0
\]
Then $f$ has a primitive $F$ in $A$.

I will only retranscribe an outline of the proof.
\begin{proof}
Since $\int_C f(z)\;dz = 0$ for every closed curve $C$, if $C_1, C_2$ are two curves going from $p\to q$ (in $A$), you have
\[
    \int_{C_1} f(z)\;dz = \int_{C_2} f(z)\;dz
\]
because $C_1\cup(-C_2)$ is a closed curve.

Define $[p, q]$ as the straight line going from $p$ to $q$, this can be parametrised by $\fonc{\gamma}{[0,1]}{A}$, $t\mapsto p + (q-p)t$.\medskip

Then fix some $p\in A$, we define $F(z)$ as:
\[
    \int_C f(w)\;dw
\]
for any curve from $p\to z$.
We want to show that $F' = f$, to this end we perturb $z$ by some $\Delta z$.
Take $\tilde{C}$ a curve from $p$ to the perturbed $z+\Delta z$, we note that $C\cup [z, z+\Delta z] \cup (-\tilde{C})$ is a closed curve in $A$.
This mean that we have:
\[
    \underbrace{\int_{\tilde{C}} f(w)\;dw}_{=F(z+\Delta z)} - \underbrace{\int_C f(w)\;dw}_{=F(z)} = \underset{[z, z+\Delta z]}{\int} f(w)\;dw = \int_0^1 f(z+t\cdot \Delta z)\cdot \Delta z\;dt
\]
Dividing by $\Delta z$ on both sides, substracting $f(z)$ you get
\[
    \left| \frac{F(z+\Delta z) - F(z)}{\Delta z} - f(z) \right| = \left| \int_0^1 f(z+t\cdot \Delta z) - f(z)\;dt \right| \leq \int_0^1 \left| f(z+t\Delta z) - f(z)\right| dt
\]
Taking the limit $|\Delta z|\to 0$ combined with some continuity arguments on $f$ yields that $F' = f$.
\end{proof}


\section{Week 4}
\subsection{Common errors}
\paragraph{Log in the complex world:}
Be careful with logarithms in $\C$.
If we are not carefull we can arrive to some "paradox", see what \emph{Johann Bernoulli} did:
\begin{flalign*}
    \log\big((-z)^2\big) = \log\big(z^2\big) &\implies \log(-z) + \log(-z) = \log(z) + \log(z) \implies 2\log(-z) = 2\log(z)\\
                                             &\implies \log(-z) = \log(z)
\end{flalign*}
Can you spot the mistake?
Read forward a bit and try to find it for yourself.\bigskip

In $\R$, we can happily define $\fonc{\exp}{\R}{\R_{>0}}$ wich---since $\exp$ is injective and surjective---is a bijection.
This mean we can define $\fonc{\log}{\R_{>0}}{\R}$ as $\log(x) := \exp^{-1}(x)$ soundly.
In $\C$, we have that $\exp(x + iy) = e^x\cdot e^{iy}$.
Let $x:= 0$, if we make $y$ bigger and bigger, we see that it loops around the circle periodicaly.
Meaning $\exp$ \emph{is not} injective in $\C$.
We cannot define directly $\log$ as its inverse.

Let us do it naivly.
Let $\log(z)$ be any complex number such that $e^{\log(z)} = z$.
Then we get:
\[
    \log(z) = \log|z| + i\arg(z)
\]
But then, since $\arg(z)$ is only defined up to a factor of $2\pi$---it takes infinitly many values---$\log(z)$ can take multiple values.
The reason is clear.
Going back to $\exp(z)$, each time $z$ goes upward by $2\pi$, $e^z$ complete a revolution around $0$.
Lets try to visualize how the mapping $\log(z)$ operates by mapping some trajectories:
\begin{center}
\begin{tikzpicture}[scale=3]
    \draw (-1,0) -- (1,0);
    \draw (0,-1) -- (0,1);
    \draw[dashed, blue, thick, -stealth] (0.4,0.4) arc (45:45-120:0.57) edge (45-126:0.57) arc (45-120:45-240:0.57) edge (45-243:0.57) arc (45-240:45-360:0.57);
    \draw[thick, -stealth] (0.4,0.4) arc (45:45+120:0.37) edge ++(254:0.005) arc (45+120:45+240:0.37) edge ++(10:0.005) arc (45+240:45+360:0.37);
    \draw[dotted, thick, -stealth, magenta] (0.4,0.4) arc (45:45+120:0.25) edge ++(254:0.005) arc (45+120:45+240:0.25) edge ++(10:0.005) arc (45+240:45+360:0.25);
    \draw[fill, red] (0.4,0.4) circle (0.4pt) node[above=0.2cm, right=0.2cm, black] {$2+2i$};

    \draw [->, line join=round, decorate, decoration={ zigzag, segment length=4, amplitude=2,post=lineto, post length=2pt }]  (1.3,0) -- (1.5,0) node[above=5, midway]{$\log(z)$};

    \draw (2,0) -- (4,0);
    \draw (3,-1) -- (3,1);
    \draw[fill, red] (3.4, 0.1) circle (0.4pt) node[above=0.2cm, right=0.2cm, black] {$w$};
    \draw[dotted, thick, -stealth, magenta] (3.4, 0.1) .. controls (3.4,0.4) and (2.6,0.4) .. (2.6, 0.1) edge ++(270:0.005) (2.6, 0.1) .. controls (2.6,-0.2) and (3.4,-0.2) .. (3.4, 0.1); 
    \draw[thick, -stealth] (3.4, 0.1) .. controls ++(0, 0.4) and ++(-0.1, -0.3) .. (2.9, 1.1);
    \draw[dashed, blue, thick, -stealth] (3.4, 0.1) .. controls ++(0, -0.6) and ++(0,0) .. (3.3, -1.1);
\end{tikzpicture}
\emph{\footnotesize Reproduced from: Visual Complex Analysis - Tristan Needham}
\end{center}
You should see later that to go around this problem we have to cut our domain and define what \emph{branches} of complex functions are---and define the main branch of the $\log$.

In summary, \emph{do not write} $\disp\int_\gamma (z-a)^{-1}\;dz = \log|z-a| \;\Big|_{\gamma(a)}^{\gamma(b)}$.


\paragraph{Interchanging $\disp\int\sum = \sum\int$:}
In general you have to justify such switch.
The meain result in this direction is \emph{Fubini-Tonelli}, one version could be stated as:

\[
    \int \sum_n f_n = \sum_n \int f_n
\]
Either if $f_n\geq 0\quad\forall n$, or if $\int \sum |f_n| < \infty$ (equivalently $\sum \int |f_n| < \infty$).


\subsection{A bit of theory}
\paragraph{Handwaving your way to Cauchy's formula:}
This is only a heuristic in order to try to see where this formula comes from:
\[
    f(z) = \frac{1}{2\pi i}\int_{\partial D_\varepsilon(z)} \frac{f(w)}{w-z} \;dw
\]
Hopefully, it gives you some hindsight.


Suppose that $f$ can be develloped as a power series around $z$, i.e.
\[
    f(w) = \sum_{k\geq 0} c_k (w-z)^k
\]
Note then that $c_0 = f(z)$.
Plug this formula in the integral term:
\begin{flalign*}
    \int_{\partial D_\varepsilon(z)} \frac{f(w)}{w-z} \;dw
        &= \int_{\partial D_\varepsilon(z)} \sum_{k\geq 0} c_k (w-z)^k\frac{1}{w-z} \;dw
        = \int_{\partial D_\varepsilon(z)} \sum_{k\geq 0} c_k (w-z)^{k-1} \;dw\\
        &= \sum_{k\geq 0} c_k \int_{\partial D_\varepsilon(z)} (w-z)^{k-1} \;dw
\intertext{We've seen that the integral term vanishes when $k-1 \neq -1$, meaning we are just left with this term to compute.}
        &= c_0 \int_{\partial D_\varepsilon(z)} (w-z)^{-1} \;dw = c_0 \cdot 2\pi i = f(z) 2\pi i
\end{flalign*}
Dividing by $2\pi i$ gives us back Cauchy's formula.
\emph{This is not a formal proof.}

\paragraph{Quick usefull formula:}
Let $\fonc{f}{\R}{\C}$ be a continuous complex-valued function.
Then you get the following upper bound on the integral:
\[
    \left|\; \int_a^b f(t)\;dt \;\right| \leq \int_a^b |f(t)|\;dt
\]
I will give only the beginning of the proof and let you fill the blanks and the correct justifications for yourselves.

\begin{proof}
Suppose $\int_a^b f(t)\;dt \neq 0$.
Let
\[
    \theta := \arg\left( \int_a^b f(t)\;dt\right)
\]
You then get that
\[
    0 < \left|\;\int_a^b f(t)\;dt\;\right| = e^{-i\theta} \int_a^b f(t)\;dt = \int_a^b \overbrace{e^{-i\theta}f(t)}^{=:h(t)} \;dt
\]
hence $h(t)\geq 0$.
\[
    \left|\;\int_a^b f(t)\;dt\;\right| = \int_a^b h(t)\;dt = \int_a^b \Re(h(t))\;dt \leq \int_a^b |h(t)|\;dt
\]
conclude.
\end{proof}

\paragraph{Possible usecase:}
This can---for instance---by used to upper bound a line integral:
\[
    \left| \int_C f(z)\;dz\;\right| \leq L(C)\cdot \max_{z\in C}|f(z)|
\]
where $L(C)$ is the \emph{length} of the curve $C$.
This can be defined as follow:

Take a parametrization $\fonc{\gamma}{[a,b]}{\C}$ of the curve $C$, then
\[
    L(C) := \int_a^b |\gamma'(t)|\;dt
\]

I will let you prove this for yoursleves.
You should also verify that the definition of length is well defined---can two parametrizations $\gamma_1, \gamma_2$ of $C$ lead to different lengths?

\section{Week 5}
\subsection{Common errors}
\paragraph{Change of variables:}
Please be carefull when doing change of variable.
In particular check your \emph{domain of integration}.
Is the variable you're integrating over real or complex?
If complex use the definition of line integral to compute.
For instance:
\[
    \int_{\partial D_R} \frac{dz}{z+1} = \int_0^{2\pi} \frac{i\cdot R\cdot e^{it}}{Re^{it}+1}\;dz = \underset{[0,2\pi]}{\int} \frac{i\cdot R\cdot e^{it}}{Re^{it}+1}\;dz
\]
Note that the interval $[0,2\pi]\subseteq \R$, meaning your variable of integration is real.
When doing the change of variable $u:=Re^{it}$, the typical mistake would be to get the new domain of integration as $[Re^{i0}, Re^{i2\pi}]=[R, R]$.
From there it is tempting to write that
\[
    i\cdot \int_R^R \frac{u}{u+1}\;du = 0
\]
since you're integrating from $R$ to $R$.
Truth is that with the change of variable you're getting right back in the complex world, $u\neq\R$ anymore.
Meaning to be correct you get directly back at some line integrale you have to compute with a parametrization.\bigskip


On a smaller note, stop leaving numbers such as $e^{i\pi}$, $e^{-3\pi i}$, $e^{i\pi / 2}$, etc. as it is in your copy.
Thoses numbers have a much simpler way of beeing written---e.g. $e^{i\pi /2} = i$.
Often it will simplify your results.


\subsection{Recap on integration-related theorems}
I will compile some of the main results you have in the lecture here.
Don't use the wrong justifications in the exercises, for instance you can't justify that some contour integral is null using Goursat if the contour is not a triangle.


\subsubsection{Goursat}
Let $U\subseteq\C$ be open, $\gamma\subseteq U$ a \emph{triangle} with interior contained in $U$.
Then for any $f\in\mathcal{H}(U)$:
\[
    \int_\gamma f(z)\;dz = 0
\]

\subsubsection{Morera}
Let $U\subseteq \C$ be open and $\fonc{f}{U}{\C}$ be continuous.
Assume that for any triangle with interior contained in $U$ we have
\[
    \int_\gamma f(z)\;dz
\]
Then $f\in\mathcal{H}(U)$.

\subsubsection{Complex version of the Fundamental theorem of calculus}
Let $U\subseteq \C$ be open, $\fonc{f}{U}{\C}$ continuous with a primitive $F\in\mathcal{H}(U)$.
If $\fonc{\gamma}{[a,b]}{U}$ is a curve in $U$, then
\[
    \int_\gamma f(z)\;dz = F(\gamma(b)) - F(\gamma(a))
\]
In particular if $\gamma$ is closed, then $\int_\gamma f(z)\;dz = 0$.


\subsubsection{Existence of primitive}
Let $\fonc{f}{D_r(z_0)}{\C}$ be holomorphic.
Then $f$ has a primitive $F\in\mathcal{H}(D_r(z_0))$.


\subsubsection{Cauchy's integral theorem}
Let $\fonc{f}{U}{\C}$ be holomorphic with $U\subseteq \C$ a \emph{simply connected open set}.
Then for any closed curve $\gamma$ in $U$, we have
\[
    \int_\gamma f(z)\;dz = 0
\]

\subsection{Small example}
Small example that uses the line integral bound of last week.
This will also come as a teaser about what's to come later in the lecture (or I hope will come).

\paragraph{The bound:}
Given a continuous complex-valued function and a (piecwise) $C^1$ curve in $\C$, you have:
\[
    \left|\;\int_\gamma f(z)\;dz\;\right| \leq L(\gamma)\cdot\max_{z\in \gamma} |f(z)|
\]\bigbreak


\begin{wrapfigure}{r}{0.4\textwidth}
\begin{tikzpicture}[scale=3.5]
    \draw[->] (-1, 0) -- (1, 0);
    \draw[->] (0, -0.3) -- (0, 1);
    \draw[thick, red, -stealth] (-0.5, 0) -- (0.5, 0) node[below, pos=0.3]{$[-R, R]$};
    \draw[thick, blue, -stealth] (0.5, 0) arc (0:180:0.5) node[right, above, pos=0.3]{$C_R^+$};
\end{tikzpicture}
\end{wrapfigure}
Let's try to use it to compute the improper integral:
\[
    \int_{-\infty}^\infty \frac{dx}{x^4+1} = \lim_{R\to \infty}\int_{-R}^R \frac{dx}{x^4+1}
\]
The idea is to see it as a complex integral.
We will close the curve $[-R, R]$ by adding the upper semi-circle $C_R^+$ to it.
Then we're integrating $(z^4 + 1)^{-1}$ on a closed curve, it has already been hinted that such integral only depends on some special points.
I will postpone the exact computation of the closed curve to later---until you have the necessary tools.
This is just a motivation to the bound on line integral I gave you :)\bigbreak


\[
    \int_{-\infty}^\infty \frac{dx}{x^4 + 1} = \underbrace{\lim_{R\to \infty}\left(\int_{-R}^R \frac{dz}{z^4 + 1} + \int_{C_R^+} \frac{dz}{z^4+1}\right)}_{=\;\pi / \sqrt{2}} - \lim_{R\to\infty} \int_{C_R^+} \frac{dz}{z^4+1}
\]
Using the inequality to bound the last term is then very easy:
\[
    \left|\;\int_{C_R^+} \frac{dz}{z^4 + 1}\;\right| \leq L(C_R^+)\cdot \max_{z\in C_R^+} \frac{1}{|z^4 + 1|} = \frac{\pi R}{R^4 - 1} \overset{R\to\infty}{\rarrow} 0
\]
Hence
\[
    \int_{-\infty}^\infty \frac{dx}{x^4+1} = \frac{\pi}{\sqrt{2}}
\]
\end{document}
