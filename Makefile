# Get all the source files
SRC := $(wildcard *.tex)

# Get the name of the dir 
OUT := $(shell pwd | sed 's/\/.*\///')
OPTS =-interaction nonstopmode -jobname $(OUT)

# Name of target pdf
TARGET := $(OUT).pdf

$(TARGET): $(SRC)
	@pdflatex $(OPTS) main.tex


%.tex:

clean:
	rm -rf *.aux *.bbl *.log *.out *.toc

compress:
		gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile="$(OUT)_compressed.pdf" "$(TARGET)"
